## RISC-V Assembly Language Syntax Highlighting
This project is a NetBeans Module Project that aims to recognize files that end with ".rvs"/".RVS". As well as providing Syntax highlighting and error detection for RISC-V assembly language.

## Installation
1. Required Platform: NetBeans IDE (12.0 or Above)
2. Clone this project into NetBeans (https://gitlab.com/darrench3140/rvsfiletype.git)
3. Right Click on Project Node and "Build"
4. Right Click on Project Node and "Install/Reload in Development IDE"
5. Restart NetBeans