lexer grammar rvsLexer;

WS						:	(' '|'\t')+ -> channel(1);
NL						:	'\r'? '\n' -> skip;
LINE_COMMENT			:	';' ~[\r\n]*;

COMMA					:	',';
COLON					:	':';
DOT						:	'.';
DB_SYMBOL				:	'db';
OPEN_SMALL_BRACKET		:	'(';
CLOSE_SMALL_BRACKET		:	')';
ADD_					:	'+';
MIN_					:	'-';
MUL_					:	'*';
DIV_					:	'/';
MOD_					:	'%';
SQU_					:	'^';

MATH_EXPRESSION			:	'-'? ('0x' [0-9a-zA-Z]+ | [0-9]+ | '0b' [01]+) (
							'0x' [0-9a-zA-Z]+
						|	[0-9]+
						|	ADD_
						|	MIN_
						|	MUL_
						|	DIV_
						|	WS
						|	OPEN_SMALL_BRACKET MATH_EXPRESSION CLOSE_SMALL_BRACKET
						)*
						;

REGISTER				:	'x' [0-9] | 'x1' [0-9] | 'x2' [0-9] | 'x3' [0-1] | 'zero' | 'ra' | 'sp' | 'gp' | 'tp' | 't' [0-6] | 's' [0-9] | 's1' [0-1] | 'fp' | 'a' [0-7]
						;

INSTRUCTION				:	[a-z]+;
