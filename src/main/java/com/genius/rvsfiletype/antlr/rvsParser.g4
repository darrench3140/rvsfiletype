parser grammar rvsParser;
options {tokenVocab = rvsLexer;}

asmfile			:	lines EOF
				;

lines			:	line*
				;

line			:	instructions
				;

imm				:	MATH_EXPRESSION;

instructions	:	INSTRUCTION REGISTER COMMA REGISTER COMMA imm
				|	INSTRUCTION REGISTER COMMA REGISTER COMMA REGISTER
				|	INSTRUCTION REGISTER COMMA imm
				|	INSTRUCTION REGISTER COMMA REGISTER
				|	INSTRUCTION imm
				|	INSTRUCTION REGISTER
				|	INSTRUCTION
				;
