/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genius.rvsfiletype.syntax;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.antlr.parser.rvsLexer;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public class rvsLanguageHierarchy extends LanguageHierarchy<rvsTokenID> {

	private final static List<rvsTokenID> tokens = new ArrayList<rvsTokenID>();
	private final static Map<Integer, rvsTokenID> idToTokens = new HashMap<Integer, rvsTokenID>();

	static {
		int index = 0;
		for (int x = 0; x <= rvsLexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = rvsLexer.VOCABULARY.getSymbolicName(x);
			if (name == null) {
				name = "INVALID" + x;
			}
			rvsTokenID token = new rvsTokenID(name, name, index);
			tokens.add(token);
			idToTokens.put(index, token);
			index++;
		}
		
		for (int x = 0; x <= rvsLexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = rvsLexer.VOCABULARY.getSymbolicName(x);
			if (name == null) {
				name = "INVALID" + x;
			}

			rvsTokenID token = new rvsTokenID("DARK_" + name, "DARK_" + name, index);
			tokens.add(token);
			idToTokens.put(index, token);
			index++;
		}
	}

	public static synchronized rvsTokenID getToken(int id) {
		return idToTokens.get(id);
	}

	public static synchronized rvsTokenID getToken(String name) {
		for (Map.Entry<Integer, rvsTokenID> entry : idToTokens.entrySet()) {
			if (entry.getValue().name().equals(name)) {
				return entry.getValue();
			}
		}
		return null;
	}

	@Override
	protected synchronized Collection<rvsTokenID> createTokenIds() {
		return tokens;
	}

	@Override
	protected synchronized Lexer<rvsTokenID> createLexer(LexerRestartInfo<rvsTokenID> info) {
		return new rvsEditorLexer(info);
	}

	@Override
	protected String mimeType() {
		return "text/x-rvs";
	}
}
