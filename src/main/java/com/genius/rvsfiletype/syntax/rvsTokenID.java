package com.genius.rvsfiletype.syntax;

import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenId;

public class rvsTokenID implements TokenId {

	private static final Language<rvsTokenID> language = new rvsLanguageHierarchy().language();
	public  String name;
	private final String primaryCategory;
	private final int id;

	public rvsTokenID(String name, String primaryCategory, int id) {
		this.name = name;
		this.primaryCategory = primaryCategory;
		this.id = id;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public int ordinal() {
		return id;
	}

	@Override
	public String primaryCategory() {
		return primaryCategory;
	}

	public static final Language<rvsTokenID> getLanguage() {
		return language;
	}

	public String toString() {
		return id + ", " + name + ", " + primaryCategory;
	}
}
