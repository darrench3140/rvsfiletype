package com.genius.rvsfiletype.syntax;

import com.genius.rvsfiletype.library.ModuleLib;
import org.antlr.parser.rvsLexer;
import org.netbeans.api.lexer.Token;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public class rvsEditorLexer implements Lexer<rvsTokenID> {

	private LexerRestartInfo<rvsTokenID> info;
	private rvsLexer lexer;

	public rvsEditorLexer(LexerRestartInfo<rvsTokenID> info) {
		this.info = info;
		rvsCharStream charStream = new rvsCharStream(info.input(), "Antlr4Editor");
		lexer = new rvsLexer(charStream);
	}

	@Override
	public Token<rvsTokenID> nextToken() {
		org.antlr.v4.runtime.Token token = lexer.nextToken();
		if (token.getType() != rvsLexer.EOF) {
			String tokenName = rvsLanguageHierarchy.getToken(token.getType()).name();

			if (ModuleLib.isDarkTheme()) {
				rvsTokenID darkTokenId = rvsLanguageHierarchy.getToken("DARK_" + tokenName);
				return info.tokenFactory().createToken(darkTokenId);
			} else {
				rvsTokenID tokenId = rvsLanguageHierarchy.getToken(tokenName);
				return info.tokenFactory().createToken(tokenId);
			}
		}
		if (info.input().readLength() > 0) {
			rvsTokenID tokenId = rvsLanguageHierarchy.getToken(rvsLexer.NL);
			return info.tokenFactory().createToken(tokenId);
		}
		return null;
	}

	@Override
	public Object state() {
		return null;
	}

	@Override
	public void release() {
	}

}
