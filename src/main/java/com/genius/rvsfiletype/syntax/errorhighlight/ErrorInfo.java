/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genius.rvsfiletype.syntax.errorhighlight;

public class ErrorInfo {

	public int offsetStart;
	public int offsetEnd;
	public String message;

	public ErrorInfo(int offsetStart, int offsetEnd, String message) {
		this.offsetStart = offsetStart;
		this.offsetEnd = offsetEnd;
		this.message = message;
	}

	@Override
	public String toString() {
		return "Error " + offsetStart + ":" + offsetEnd + "\t" + message;
	}
}
