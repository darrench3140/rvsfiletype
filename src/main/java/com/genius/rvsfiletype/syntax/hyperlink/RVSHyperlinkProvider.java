package com.genius.rvsfiletype.syntax.hyperlink;

import com.genius.rvsfiletype.syntax.parser.MyRVSParser;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.antlr.v4.runtime.Token;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProvider;

@MimeRegistration(mimeType = "text/x-rvs", service = HyperlinkProvider.class)
public class RVSHyperlinkProvider implements HyperlinkProvider {

	@Override
	public boolean isHyperlinkPoint(Document dcmnt, int i) {
		for (Token token : MyRVSParser.allTokens) {
			if (token.getStartIndex() <= i && i <= token.getStopIndex() && !token.getText().equals(",") && !token.getTokenSource().toString().equals("WS")) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int[] getHyperlinkSpan(Document dcmnt, int i) {
		for (Token token : MyRVSParser.allTokens) {
			if (token.getStartIndex() <= i && i <= token.getStopIndex()) {
				return new int[]{token.getStartIndex(), token.getStopIndex() + 1};
			}
		}
		return null;
	}

	@Override
	public void performClickAction(Document dcmnt, int i) {
		JTextComponent jTextComponent = EditorRegistry.lastFocusedComponent();
		jTextComponent.setCaretPosition(10);
	}
}