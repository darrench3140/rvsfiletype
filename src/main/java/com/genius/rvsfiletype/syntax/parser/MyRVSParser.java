package com.genius.rvsfiletype.syntax.parser;

import com.genius.rvsfiletype.library.ModuleLib;
import java.io.File;
import java.util.ArrayList;
import javax.swing.event.ChangeListener;
import org.antlr.parser.rvsLexer;
import org.antlr.parser.rvsParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;

public class MyRVSParser extends Parser {

	private Snapshot snapshot;
	private ParseResult result;
	
	public static ArrayList<Token> allTokens = new ArrayList<>();

	@Override
	public void parse(Snapshot snapshot, Task task, SourceModificationEvent sme) throws ParseException {
		ModuleLib.log("rvsParser::parse()");
		this.snapshot = snapshot;
		String content = snapshot.getText().toString();
		File folder = new File(snapshot.getSource().getFileObject().getParent().getPath());
		System.out.println("snapshot.getSource().getFileObject().getPath()=" + snapshot.getSource().getFileObject().getPath());
		System.out.println("folder=" + folder.getAbsolutePath());
		RVSErrorListener errorListener = new RVSErrorListener();
		rvsLexer lexer = new rvsLexer(CharStreams.fromString(content));
		allTokens.clear();
		for (Token token : lexer.getAllTokens()) {
			allTokens.add(token);
		}
		lexer.reset();
		lexer.addErrorListener(errorListener);
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		rvsParser parser = new rvsParser(tokenStream);
		parser.addErrorListener(errorListener);
		rvsParser.AsmfileContext context = parser.asmfile();
		ParseTreeWalker walker = new ParseTreeWalker();
		RVSParserListener listener = new RVSParserListener(parser);
		walker.walk(listener, context);
		result = new ParseResult(snapshot, listener, errorListener);
	}

	@Override
	public Result getResult(Task task) throws ParseException {
		return result;
	}

	@Override
	public void addChangeListener(ChangeListener cl) {
	}

	@Override
	public void removeChangeListener(ChangeListener cl) {
	}

}
