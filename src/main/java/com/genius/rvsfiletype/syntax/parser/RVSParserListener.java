package com.genius.rvsfiletype.syntax.parser;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.antlr.v4.runtime.Parser;

public class RVSParserListener extends RVSParserBaseListener{

	private final Map<String, Integer> rmap = new HashMap<>();

	public RVSParserListener(Parser parser) {
		rmap.clear();
		rmap.putAll(parser.getRuleIndexMap());
	}

	public String getRuleByKey(int key) {
		return rmap.entrySet().stream()
				.filter(e -> Objects.equals(e.getValue(), key))
				.map(Map.Entry::getKey)
				.findFirst()
				.orElse(null);
	}
}
