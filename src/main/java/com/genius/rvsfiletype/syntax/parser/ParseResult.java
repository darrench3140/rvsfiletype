package com.genius.rvsfiletype.syntax.parser;

import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;

public class ParseResult extends Parser.Result {

	public RVSParserListener listener;
	public RVSErrorListener errorListerner;

	public ParseResult(Snapshot _snapshot, RVSParserListener listener, RVSErrorListener errorListerner) {
		super(_snapshot);
		this.listener = listener;
		this.errorListerner = errorListerner;
	}

	@Override
	protected void invalidate() {

	}

}
