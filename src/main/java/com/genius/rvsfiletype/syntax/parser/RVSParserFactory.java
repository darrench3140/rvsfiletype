package com.genius.rvsfiletype.syntax.parser;

import java.util.Collection;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.ParserFactory;

public class RVSParserFactory extends ParserFactory {

	@Override
	public org.netbeans.modules.parsing.spi.Parser createParser(Collection<Snapshot> clctn) {
		return new MyRVSParser();
	}

}
