# Syntax Highlighting
1. Modify pom.xml, add antlr4-maven-plugin (link up parser and lexer) and dependencies
2. Right Click project node, create new File Type
3. Write Lexer and Parser files (in rvsfiletype.antlr)
4. Right CLick project node, create new XML Layer
5. copy files from rvsfiletype.library
6. copy files from rvsfiletype.syntax (modify yourself)
7. create FontAndColors.xml in src/main/resources/.../syntax/
8. modify your colors (now running this project should be able to give colors)
# Error Detection
9. copy files from rvsfiletype.syntax.errorhighlight and parser